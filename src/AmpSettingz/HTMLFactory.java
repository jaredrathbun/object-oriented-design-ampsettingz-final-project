package AmpSettingz;

import java.awt.Dimension;
import java.io.IOException;
import java.net.URL;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

/**
 * <pre>
 * Author: Jared Rathbun
 * Course: CSC2620
 *
 * This class serves are a factory, using the Singleton design pattern to 
 * return instances of the specified object, which contains components that use 
 * HTML.
 * </pre>
 */
public final class HTMLFactory
{
    /**
     * Private Constructor which is does not change any data.
     */
    private HTMLFactory()
    {
        // Do nothing, private constructor.
    }

    /**
     * This method returns an instance of the IntructionsPanel, which holds 
     * instructions on how to use AmpSettingz.
     * 
     * @return A JScrollPane containing the instructions.
     */
    public static JScrollPane getInstructionsPanelInstance()
    {
        return InstructionsPanel.getInstance();
    }

    /**
     * This method returns an instance of the BasicInformationPanel, which 
     * holds basic information about the program.
     * 
     * @return A JScrollPane containing the basic information.
     */
    public static JScrollPane getBasicInfoPanelInstance()
    {
        return BasicInfoPanel.getInstance();
    }

    /**
     * This class servers as a holder for the InstructionsPanel, which reads a
     * .html file from the project directory and creates a JScrollPane from it.
     */
    private final static class InstructionsPanel
    {
        private static final String HTML_FILE = "instructions.html";
        private static JEditorPane contentsPane;
        private static JScrollPane scrollPane;

        /**
         * Private constructor which does not affect any data.
         */
        private InstructionsPanel()
        {
            // Do nothing, private constructor.
        }

        /**
         * This method returns a static instance of the instructions on a 
         * JScrollPane.
         * 
         * @return A JScrollPane containing the instructions.
         */
        public static JScrollPane getInstance()
        {
            // Initialize the JEditorPane.
            contentsPane = new JEditorPane();
            contentsPane.setEditable(false);

            // Get the URL object from the file name.
            final URL URL = HTMLFactory.class.getResource(HTML_FILE);

            // Try to set the page, and catch the exception if thrown.
            try
            {
                contentsPane.setPage(URL);
            } catch (IOException e)
            {
                contentsPane.setContentType("text/html");
                contentsPane.setText("<html>Page not found.</html>");
            }

            // Initialize the JScrollPane and nest the JEditorPane in it.
            scrollPane = new JScrollPane(contentsPane);
            scrollPane.setPreferredSize(new Dimension(1050, 600));

            return scrollPane;
        }
    }

    /**
     * This class serves as a holder for the Basic Information Pane, which holds
     * information on the functionality of the program.
     */
    private final static class BasicInfoPanel
    {
        private static final String HTML_FILE = "basicinfo.html";
        private static JEditorPane contentsPane;
        private static JScrollPane scrollPane;

        /**
         * Private constructor which does not affect any data.
         */
        private BasicInfoPanel()
        {
            // Do nothing, private constructor.
        }

        /**
         * This method gets the static instance of the BasicInfoPanel, 
         * returning a JScrollPane which holds the basic information.
         * 
         * @return A JScrollPane containing the Basic Information.
         */
        public static JScrollPane getInstance()
        {
            // Initialize the JEditorPane.
            contentsPane = new JEditorPane();
            contentsPane.setEditable(false);

            // Get the URL object from the file name.
            final URL URL = HTMLFactory.class.getResource(HTML_FILE);

            // Try to set the page, and catch the exception if thrown.
            try
            {
                contentsPane.setPage(URL);
            } catch (IOException e)
            {
                contentsPane.setContentType("text/html");
                contentsPane.setText("<html>Page not found.</html>");
            }

            // Initialize the JScrollPane and nest the JEditorPane in it.
            scrollPane = new JScrollPane(contentsPane);
            scrollPane.setPreferredSize(new Dimension(1050, 600));

            return scrollPane;
        }
    }
}

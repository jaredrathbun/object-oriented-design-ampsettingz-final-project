package AmpSettingz;

/**
 * <pre>
 * Authors: Jared Rathbun, Alexander Royer and Samuel Dalke
 * Course: CSC2620
 * Due Date: 12-4-20
 *
 * Initial commit, this class serves are the entry point to the program.
 * </pre>
 */
public class Driver
{
    private static final String LOCAL_HOST = "127.0.0.1";
    private static final int PORT = 5000;

    /**
     * Entry point to the program. Creates the Server and Window Objects and
     * starts the GUI.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args)
    {
        boolean deleteVal = false;
        
        if (args.length != 1)
        {
            System.err.println("Usage: AmpSettingz [-tabledeletionflag]");
            return;
        }
        else
            deleteVal = Boolean.valueOf(args[0]);
        
        // Create a new Server object.
        Server server = new Server(PORT);
        server.start();

        // Create a new Window object.
        Thread windowThread = new Thread(new Window(LOCAL_HOST, PORT, server,
                deleteVal));
        windowThread.start();
    }
}

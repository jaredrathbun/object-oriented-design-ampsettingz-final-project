package AmpSettingz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <pre>
 * Authors: Jared Rathbun, Alexander Royer, and Samuel Dalke
 * Course: CSC2620
 * Due Date: 12-4-20
 *
 * This class serves as the Server to the whole project. It interacts with the
 * SQL Database and allows operations such as editing, adding, and removing 
 * entries.
 * </pre>
 */
public class Server extends Thread
{
    private int port;
    private Socket windowSocket = null;
    private ServerSocket serverSocket = null;
    private final String DB_HOST = "jdbc:derby://localhost:1527/AmpSettingz";
    private Connection dbConnection;
    private Statement stmt;
    private ResultSet rs;
    private int IDCount = 0;
    
    public ArrayList<WindowThread> windowList;
    
    /**
     * Constructor that takes the Port in order to construct the ServerSocket 
     * object.
     * 
     * @param port The port number. 
     */
    public Server(int port)
    {
        this.port = port;
        windowList = new ArrayList<>();
        
        // Create a new ServerSocket.
        try
        {
            serverSocket = new ServerSocket(this.port);
        } catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Error creating server.");
        }
        
        // Connect the Server to the SQL Database.
        connectToDB();
    }
    
    /**
     * This method overrides from the Thread class and allows the ServerSocket 
     * to accept information from the client.
     */
    @Override
    public void run()
    {
        /* An infinite loop that searches for a new connection to the server. 
        If the connection is found, create a new WindowThread and start it. */
        while (true)
        {
            try
            {
                windowSocket = serverSocket.accept();
                WindowThread newWindow = new WindowThread(windowSocket);
                newWindow.start();
                windowList.add(newWindow);
            } catch (IOException e)
            {
                System.err.println("Connection to window cannot be established."
                        + e.getMessage());
                
                break;
            } catch (NullPointerException e)
            {
                JOptionPane.showMessageDialog(null, "Error creating server. "
                        + "Another instance of the program is running.");
                System.exit(0);
                break;
            }
            
        }
    }
    
    /**
     * This method truncates the database file.
     */
    public void dispatchDB()
    {
        String query = "DELETE \n FROM AmpSettingz";
        
        try
        {
            stmt.executeUpdate(query);
        } catch (SQLException e)
        {
            System.err.println("Error truncating table: -> " + e.getMessage());
            return;
        }

        System.out.println("Successfully truncated table.");
        
    }
    
    /**
     * This method posts the 2D Array of data to all of the active Windows.
     * 
     * @param data The 2D Array of data collected from the SQL database. 
     */
    public void postToAllWindows(Object[][] data)
    {
        windowList.get(0).post(data);
    }
    
    /**
     * This method connects this class to the SQL database by using the 
     * Connection object.
     */
    private void connectToDB()
    {
        String username = "root";
        String password = "root";
        
        try 
        {
            dbConnection = DriverManager.getConnection(DB_HOST, username, 
                    password);
        } catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
    }
    
    /**
     * This method executes a query on the database.
     * 
     * @param query The query to execute. 
     * @param operation The name of the operation to perform.
     */
    public void executeQuery(String query, String operation)
    {
        int res;
        
        try
        {
            if (operation.equals("SELECT"))
                rs = stmt.executeQuery(query);
            else
                res = stmt.executeUpdate(query);
            
        } catch (SQLException ex) 
        {
            System.err.println(ex.getMessage());
        }
      
        // Post the data to all Windows.
        postToAllWindows(dbToArray());
    }
    
    /**
     * This method saves the database to a file with a .csv style.
     */
    public void saveDB()
    {
        // The PrintStream object.
        PrintStream ps = null;
        
        // The File object.
        File file = null;
        
        // Create tje JFileChooser object.
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileNameExtensionFilter("AmpSettting (.as)",
                "as"));
        
        // Create the File object.
        if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            file = new File(jfc.getSelectedFile() + ".as");
        }
            
        try 
        {
            ps = new PrintStream(file);
            Object[][] data = dbToArray();
            
            // Loop over each array in the 2D array and write it to the file.
            for (int i = 0; i < data.length; i++)
            {
                final Object ID = data[i][0];
                final Object REVERB = data[i][1];
                final Object GAIN = data[i][2];
                final Object PRESENCE = data[i][3];
                final Object MIDDLE = data[i][4];
                final Object BASS = data[i][5];
                final Object ARTIST_NAME = data[i][6];
                final Object AMP_NAME = data[i][7];
                
                ps.println(ID + ", " + REVERB + ", " + GAIN + ", " + PRESENCE +
                        ", " + MIDDLE + ", " + BASS + ", '" + ARTIST_NAME + 
                        "',' " + AMP_NAME + "'");
            }
            
            JOptionPane.showMessageDialog(null, "Successfully saved file.");
        } catch (FileNotFoundException e) 
        {
            JOptionPane.showMessageDialog(null, "Error saving file.\nReason: "
                    + "FileNotFoundException");
        } catch (NullPointerException e)
        {
            JOptionPane.showMessageDialog(null, "Saving Cancelled.");
        } finally 
        { 
            // Close the PrintStream.
            if (ps != null)
                ps.close();
        } 
    }
    
    /**
     * This method loads the database from a .as file.
     */
    public void loadDB()
    {
        // The File object.
        File file = null;
        
        // Create tje JFileChooser object.
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileNameExtensionFilter("AmpSettting (.as)",
                "as"));
        
        // Create the File object.
        if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            file = jfc.getSelectedFile();
        
        if (file != null)
        {
            try 
            {
                // A BufferedReader object.
                BufferedReader br = new BufferedReader(new FileReader(file));
                
                // A String for each line in the file.
                String line;
                
                try 
                {
                    while ((line = br.readLine()) != null)
                    {  
                        IDCount++;

                        executeQuery("INSERT INTO AmpSettingz(ID, REVERB, GAIN,"
                               + " PRESENCE, MIDDLE, BASS, ARTIST_NAME,"
                               + " AMP_NAME) \n"+ "VALUES (" + line + ")", 
                                "INSERT"); 
                    }
                } catch (IOException e) 
                {
                    System.err.println("IOException while reading file." + 
                            e.getMessage());
                }
            } catch (FileNotFoundException e) 
            {
                JOptionPane.showMessageDialog(null, "Unable to read file: " + 
                        file.getName());            
            }
        }
    }
    
    /**
     * This method creates a new Printer object using the data from the 
     * database.
     * 
     * @see Printer
     */
    public void printDB() 
    {
        // Create a new Printer object using the data in the database.
        new Printer(dbToArray());
    }
    
    /**
     * This method gets the current ID count from the database.
     * 
     * @return An integer containing the current ID. 
     */
    public int getID()
    {
        String query = "SELECT * FROM AmpSettingz";
        int maxID = 0;
        
        try
        {
            rs = stmt.executeQuery(query);
            
            while (rs.next())
            {
                int currentID = rs.getInt("ID");
                maxID = (currentID > maxID) ? currentID : maxID;
            }
        } catch (SQLException ex)
        {
            System.err.println("Error getting getting database from "
                    + "statement.");
        }
        
        return maxID;
    }
    
    /**
     * This method increments the IDCount by one.
     */
    public void incrementID()
    {
        IDCount++;
    }
    
    /**
     * This method converts the SQL Database into a 2D Array of Object.
     * 
     * @return A 2D Array containing the elements of the SQL Database. 
     */
    public Object[][] dbToArray()
    {
        ArrayList<Object[]> list = new ArrayList<>();
        
        String query = "SELECT * FROM AMPSETTINGZ";
        try
        {
            // Create a Statement and ResultSet from the query.
            stmt = dbConnection.createStatement();
            rs = stmt.executeQuery(query);
            
            // Loop over every element in the database.
            while (rs.next())
            {
                Object[] row = new Object[8];
                row[0] = rs.getInt("ID");
                row[1] = rs.getInt("REVERB");
                row[2] = rs.getInt("GAIN");
                row[3] = rs.getInt("PRESENCE");
                row[4] = rs.getInt("MIDDLE");
                row[5] = rs.getInt("BASS");
                row[6] = rs.getString("ARTIST_NAME").trim();
                row[7] = rs.getString("AMP_NAME").trim();
                
                list.add(row);
            }
        } catch (SQLException e)
        {
            System.err.println("Error creating statement from connection." + 
                    e.getSQLState() + "\n" + e.getMessage());
        }
        
        // Allocate a new 2D Array with the size of the list.
        Object[][] array = new Object[list.size()][8];
        
        // Fill the array.
        for (int i = 0; i < list.size(); i++)
            array[i] = list.get(i);
        
        return array;
    }
    
    /**
     * This method gets the row of data from the database at the specified ID.
     * 
     * @param ID The ID of the row.
     * @return An array of Objects that contains the data in the row. 
     */
    public Object[] getRow(int ID)
    {
        Object[][] table = dbToArray();
        
        for (Object[] row : table)
        {
            if (Integer.valueOf(String.valueOf(row[0])) == ID)
                return row;
        }
        
        return null;
    }
    
    /**
     * This method returns whether or not the database has data in it.
     * 
     * @return true if the database has data, false otherwise. 
     */
    public boolean hasData()
    {
        return (dbToArray() != null);
    }

    /**
     * This class serves as a helper class between the Window and Server 
     * classes.
     */
    private class WindowThread extends Thread
    {
        // IO Streams.
        private ObjectInputStream inputStream;
        private ObjectOutputStream outputStream;
        
        /**
         * Constructor which takes a Socket to connect to.
         * 
         * @param connection The Socket to connect to. 
         */
        public WindowThread(Socket connection)
        {
            try
            {
                inputStream = new ObjectInputStream(connection
                        .getInputStream());
                outputStream = new ObjectOutputStream(connection
                        .getOutputStream());
            } catch (IOException e)
            {
                System.err.println("Error with IO at server. Cannot create "
                        + "object stream.");
            }
        }
        
        /**
         * This method overrides from the Thread class and constantly loops 
         * until the ObjectInputStream reads incoming data.
         */
        @Override
        public void run()
        {
            while (true)
            {
                Object[][] data = null;
                
                try
                {
                    data = (Object[][]) inputStream.readObject();
                } catch (IOException | ClassNotFoundException e)
                {
                    System.err.println(e.getMessage());
                }
                
                // Post the data to all Windows.
                postToAllWindows(data);
            }
        }
        
        /**
         * This method posts the 2D Array of data specified to the 
         * ObjectOutputStream.
         * 
         * @param data The 2D Array of data to post to the ObjectOutputStream.
         */
        public void post(Object[][] data)
        {
            try
            {
                outputStream.writeObject(data);
                outputStream.flush();
            } catch (IOException e)
            {
                System.err.println("Error writing data to Windows");
            }
            
        }
    }
}

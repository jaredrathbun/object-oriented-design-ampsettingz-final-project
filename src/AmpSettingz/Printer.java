package AmpSettingz;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Date;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;

/**
 * <pre>
 * Authors: Jared Rathbun, Alexander Royer and Samuel Dalke
 * Course: CSC2620
 * Due Date: 12-4-20
 *
 * This class serves as an object that will print a 2D Array of data onto a 
 * piece of 8.5" x 11" paper.
 * 
 * CODE REFERENCED FROM:
 * <a href="https://forums.codeguru.com/showthread.php?511168-Set-page-margins-
 * when-printing-in-java#:~:text=The%20Java%20Print%20API%20doesn,the%20trouble
 * %20of%20searching...">Referenced Code</a>
 * </pre>
 */
public class Printer implements Printable
{
    Object[][] data;
    String lines;
    private Date printDate = new Date();
    
    /**
     * Constructor which uses a 2D Array of data to print onto a piece of paper.
     * 
     * @param data The 2D Array of data to print. 
     */
    public Printer(Object[][] data)
    {
        this.data = data;
        lines = convertArrayToString();
        
        // Get the job from the getPrinterJob() method.
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = job.defaultPage();
        
        // Define a new Paper object and set margins.
        Paper paper = new Paper();
        double margin = 36;
        paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, 
                paper.getHeight() - margin * 2);
        pf.setPaper(paper);
        
        // Add attributes to the paper so it prints landscape.
        PrintRequestAttributeSet attributes = 
                new HashPrintRequestAttributeSet();
        attributes.add(OrientationRequested.LANDSCAPE);
        job.setPrintable(this, pf);
        boolean ok = job.printDialog();
        
        // Attempt to print the job if the dialog result is okay.
        if (ok)
        {
            try
            {
                pf.setOrientation(PageFormat.LANDSCAPE);
                job.print(attributes);
            } catch (PrinterException ex)
            {
                // The job did not successfully complete.
            }
        }
    }

    /**
     * This method prints the String onto the Book.
     * 
     * @param g The Graphics object.
     * @param pf The PageFormat object.
     * @param page The number of pages.
     * 
     * @return An integer containing the result of the operation.
     * @throws PrinterException If the job does not successfully complete.
     */
    @Override
    public int print(Graphics g, PageFormat pf, int page) 
            throws PrinterException {
        if (page > 0)
            return NO_SUCH_PAGE;
        
        // Get the Graphics2D object and translate it so it can print on paper.
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableY(), pf.getImageableX());
        
        // Print the date as a header.
        g2d.setFont(new Font(Font.MONOSPACED, Font.BOLD, 8));
        g2d.drawString("PRINT DATE: " + printDate.toString(), 15, 5);

        // Print the data.
        g.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 8));
        drawString(g, lines, 15, 15);
        
        return PAGE_EXISTS;
    }
    
    /**
     * This method draws the specified message and splits the new line 
     * character.
     * 
     * @param g The Graphics object.
     * @param message The message to print.
     * @param x The x location.
     * @param y The y location.
     */
    private void drawString(Graphics g, String message, int x, int y)
    {
        for (String line : message.split("\n"))
            g.drawString(line, x, y += g.getFontMetrics().getHeight());
    }

    /**
     * This method converts the 2D Array to a String, pretty printed with the
     * String.format() method.
     * 
     * @return A String representation of the elements of the 2D Array. 
     */
    private String convertArrayToString() 
    {
        String line =  "ID |   REVERB |   GAIN |   PRESENCE |    MIDDLE |"
                + "     BASS |           ARTIST NAME |                  AMPLI"
                + "FIER "
                + "NAME |                       NOTES|\n---+----------+-------"
                + "-+------------+-----------+----------+---------"
                + "--------------+---------------------------------+----------"
                + "------------------+\n";
        
        // Loop over every entry in the array and append it to line.
        for (int i = 0; i < data.length; i++)
        {
            String currentLine = "";
            
            currentLine = String.format("%2d |       %2d |     %2d |      "
                    + "   %2d |        %2d |       %2d |  %20s |  %30s | "
                    + "________________________________\n", data[i][0], 
                    data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], 
                    data[i][6], data[i][7]);
            line += currentLine;
        }
        
        return line;
    }  
}

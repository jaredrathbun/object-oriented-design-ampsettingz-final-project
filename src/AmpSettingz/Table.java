package AmpSettingz;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JPanel;

/**
 * <pre>
 * Authors: Jared Rathbun, Alexander Royer, Samuel Dalke
 * Course: CSC2620
 * Due Date: 12-4-20
 *
 * This class holds the code needed to generate and display a Table.
 * </pre>
 */
public class Table extends JPanel
{
    private final Object[][] DATA;
    private final String header = "ID |   REVERB |   GAIN |   PRESENCE |    "
                + "MIDDLE |     BA"
                + "SS |                ARTIST NAME |               AMPLIFIER"
                + " NAME |\n---+----------+--------+------------+-----------+--"
                + "--------+----------------------------+----------------------"
                + "--------+\n";
    
    /**
     * Constructor which uses the specified 2D Array of Objects to print to the 
     * Table.
     * 
     * @param data The 2D Array of Objects (data read from database).
     */
    public Table(Object[][] data)
    {
        // If the data is null or the length of the array is 0, fill fake data.
        if (data == null || data.length == 0)
           this.DATA = new Object[][]{{1, 0, 0, 0, 0, 0, "No Data Entered", 
                "No Data Entered"}};    
        else
            this.DATA = data;
        
        // Set the size, layout, and ignore repainting.
        setSize(1000, 620);
        setLayout(null);
        setIgnoreRepaint(true);
    }

    /**
     * paintComponent() method overridden from JPanel. Draws a header and the 
     * formatted data.
     * 
     * @param g The Graphics Object.
     */
    @Override
    public void paintComponent(Graphics g)
    {
        // Print the header.
        g.setFont(new Font("monospaced", Font.BOLD, 14));
        Point headerPoint = drawString(g, header, 5, 0);
        
        // Set the font and get the text, then print.
        g.setFont(new Font("monospaced", Font.PLAIN, 14));
        drawString(g, convertArrayToString(), headerPoint.x, headerPoint.y);
        
        // Dipose of the Graphics object.
        g.dispose();
    }
    
    /**
     * This method draws the specified message and splits the new line 
     * character.
     * 
     * @param g The Graphics object.
     * @param message The message to print.
     * @param x The x location.
     * @param y The y location.
     */
    private Point drawString(Graphics g, String message, int x, int y)
    {
        for (String line : message.split("\n")) 
            g.drawString(line, x, y += g.getFontMetrics().getHeight());
        
        return new Point(x, y);
    }

    /**
     * This method converts the 2D Array to a String, pretty printed with the
     * String.format() method.
     * 
     * @return A String representation of the elements of the 2D Array. 
     */
    private String convertArrayToString() 
    {
        String line = "";
        
        /* Loop over every entry in the array and append it to line, formatting 
        every line. */
        for (Object[] array : DATA)
        {
            String currentLine = "";
            currentLine = String.format("%2d |       %2d |     %2d |      "
                    + "   %2d |        %2d |       %2d |  %25s |  %27s | \n", 
                    array[0], array[1], array[2], array[3], array[4], array[5],
                    array[6], array[7]);
            
            line += currentLine;
        }
        
        return line;
    } 
}
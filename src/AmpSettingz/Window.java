package AmpSettingz;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.imageio.ImageIO;

import java.net.Socket;

import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;

/**
 * <pre>
 * Authors: Jared Rathbun, Alexander Royer and Samuel Dalke
 * Course: CSC2620
 * Due Date: 12-4-20
 *
 * This class holds all of the GUI components needed for the project. It allows
 * the user to edit the database, add new settings, and print the database.
 * </pre>
 */
public class Window extends JFrame implements Runnable
{

    private Socket connectionSocket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private final Server SERVER;
    private final String IP;
    private final int PORT;
    private JPanel creationPanel, buttonsPanel,
            basicInstructionsPanel, aboutPanel, bottomPanel, cards;
    private boolean isTablePanelVisible, isCreationPanelVisible,
            isAboutPanelVisible, isBasicInstructionsPanelVisible;
    private JButton newButton, editButton, removeButton, printButton,
            exitButton, tableButton;
    private JMenuBar menuBar;
    private JScrollPane tableScrollPane;
    private Table table;
    private static GridBagConstraints gbc;
    private final String TABLE_PANEL_ID = "TABLE_PANEL";
    private final String CREATION_PANEL_ID = "CREATION_PANEL";
    private final String BASIC_INSTRUCTION_ID = "BASIC_SINTRUCTION";
    private final String ABOUT_ID = "ABOUT";
    private final String reverbDescription = "Reverb is a persistence of sound."
            + " When this is added to a signal it can add a \"wavy\" effect, "
            + "otherwise known as \"chorus\". This is noticable when a sound "
            + "stops but the reflections of the sound continues.";
    private final String gainDescription = "Gain is the how loud something is "
            + "before it goes through an processing (ex. Amplifier, Recording "
            + "Equipment.) This is commmonly referred to as how much \"metal\" "
            + "sound is present.";
    private final String presenceDescription = "Presence is the control of the "
            + "upper mid-range boosts frequencies that make up the sounds an "
            + "amplifier produces. This causes similar tone ranges to sound "
            + "more present.";
    private final String middleDescription = "The Middle control effects how "
            + "many mid-range frequences are present in a sound. As the Middle"
            + " is increased, the number of mid-range frequences increases.";
    private final String bassDescription = "The Bass control effects the number"
            + " of bass-range frequencies that are present in a sound. Bass is "
            + "the deepest level of sound.";

    /**
     * Constructor which creates the JFrame object, sets GUI attributes, and 
     * adds a WindowListener to the GUI if the truncationFlag is true.
     * 
     * @param IP The IP Address.
     * @param port The Port Address.
     * @param server The Server object.
     * @param truncationFlag A flag which signals whether or not the SQL 
     * Database is to to be truncated or not after execution of the program.
     */
    public Window(String IP, int port, Server server, boolean truncationFlag)
    {
        super("AmpSettingz");

        setSize(1250, 800);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setVisible(true);

        this.IP = IP;
        this.PORT = port;
        this.SERVER = server;

        if (truncationFlag)
        {
            addWindowListener(new WindowAdapter()
            {
                @Override
                public void windowClosing(WindowEvent e)
                {
                    SERVER.dispatchDB();
                    e.getWindow().dispose();
                }
            });
        }

        // Call methods to populate the Window. 
        setFrameIconAndBanner();
        setMenu();
        initComponents();

        // Add it to the cards.
        add(cards, BorderLayout.CENTER);

        // refresh GUI.
        validate();
    }

    /**
     * Run method to connect to create a socket and connect to a Socket.
     */
    @Override
    public void run()
    {
        try
        {
            // Construct the socket for the connection to the SERVER.
            connectionSocket = new Socket(IP, PORT);

            // Construct the ObjectOutput/ObjectInput streams.
            outputStream = new ObjectOutputStream(connectionSocket
                    .getOutputStream());
            inputStream = new ObjectInputStream(connectionSocket
                    .getInputStream());
        } catch (IOException e)
        {
            System.err.println("Cannot start connection at: " + IP + ":"
                    + PORT + "\n" + e.getMessage());
        }

        /* If the SERVER sends a new set of data to the stream, rebuild the 
        JTable. */
        while (true)
        {
            try
            {
                Object[][] data = (Object[][]) inputStream.readObject();
                tableScrollPane = new JScrollPane(new Table(data));
                validate();
            } catch (IOException | ClassNotFoundException e)
            {
                System.err.println("");
            }

        }
    }

    /**
     * Initialize some GUI components in memory. Initialize table, creation,
     * buttons, about, and basic instructions panel.
     */
    private void initComponents()
    {
        cards = new JPanel(new CardLayout());
        gbc = new GridBagConstraints();

        initTablePanel();
        initCreationPanel();
        initButtonsPanel();
        initAboutPanel();
        initBasicInstructionsPanel();

        bottomPanel = new JPanel(new GridBagLayout())
        {
            {
                setBackground(Color.LIGHT_GRAY);
                JLabel versionLabel = new JLabel("AmpSettingz - V1.0 ");
                versionLabel.setForeground(Color.RED);

                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.EAST;
                gbc.insets = new Insets(2, 2, 2, 2);
                add(versionLabel, gbc);
            }
        };
        // Add bottomPanel to the GUI.
        add(bottomPanel, BorderLayout.SOUTH);
        
        // Refresh the GUI.
        validate();
    }

    /**
     * Creates the table JPanel.
     */
    private void initTablePanel()
    {
        // Reset the tablePanel if it has been created previously.
        if (isTablePanelVisible)
        {
            remove(tableScrollPane);
            tableScrollPane = null;
        }

        // Create a table that is populated with items from the database if
        // there is data, else create an empty table.
        if (SERVER.hasData())
        {
            table = new Table(SERVER.dbToArray());
        } else
        {
            table = new Table(null);
        }
        
        // Create a new JScrollPane and popualte it with the table.
        tableScrollPane = new JScrollPane(table);
        
        // Set the size to 1000 x 620.
        tableScrollPane.setPreferredSize(new Dimension(1000, 620));

        // Add the scroll pane to the cards.
        cards.add(tableScrollPane, TABLE_PANEL_ID);
    }

    /**
     * Creates the JPanel that is used to create a new AmpSetting.
     */
    private void initCreationPanel()
    {
        // Reset the creationPanel if it has been called previously. 
        if (isCreationPanelVisible)
        {
            remove(creationPanel);
            creationPanel = null;
        }

        // Create a new creation panel that uses GBL.
        creationPanel = new JPanel(new GridBagLayout());

        // Create a Hashtable for the labels of the sliders.
        Hashtable labels = new Hashtable();
        for (int i = 0; i <= 10; i++)
        {
            labels.put(i, new JLabel(String.valueOf(i)));
        }

        /* [0] = "Reverb", [1] = "Gain", [2] = "Presence", [3] = "Middle", 
        [4] = "Bass" */
        final int[] selectedVals = new int[5];
        for (int i = 0; i < selectedVals.length; i++)
        {
            selectedVals[i] = 5;
        }

        // Create a JSlider that gets populated with the sliderArray.
        final JSlider[] sliderArray = new JSlider[5];
        
        // Create the JPanel to adjust Equalisation.
        JPanel eqPanel = new JPanel()
        {
            {
                setBorder(BorderFactory.createTitledBorder("Equalisation"));

                gbc.insets = new Insets(2, 2, 2, 2);

                // A panel for the reverb setting.
                JPanel reverbPanel = new JPanel(new GridBagLayout());
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;
                reverbPanel.add(new JLabel("<HTML><U> Reverb </U></HTML>"),
                        gbc);
                // Displays the description of Reverb.
                JTextArea description = new JTextArea(reverbDescription);
                description.setEditable(false);
                description.setLineWrap(true);
                description.setWrapStyleWord(true);
                description.setSize(200, 230);
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.anchor = GridBagConstraints.CENTER;
                reverbPanel.add(description, gbc);
                gbc.gridx = 0;
                gbc.gridy = 2;
                gbc.anchor = GridBagConstraints.CENTER;
                sliderArray[0] = new JSlider(0, 10, 5);
                sliderArray[0].setLabelTable(labels);
                sliderArray[0].setPaintLabels(true);
                sliderArray[0].setPaintTicks(true);
                sliderArray[0].addChangeListener((ChangeEvent e) ->
                {
                    // Get the current value of the Slider.
                    selectedVals[0] = ((JSlider) e.getSource()).getValue();
                });
                
                // Add the sliderArray at pos 0 to the reverbPanel.
                reverbPanel.add(sliderArray[0], gbc);
                // Add the reverbPanel to the GUI.
                add(reverbPanel);

                // A panel for the gain setting.
                JPanel gainPanel = new JPanel(new GridBagLayout());
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;
                gainPanel.add(new JLabel("<HTML><U> Gain </U></HTML>"),
                        gbc);
                // Display the description of Gain.
                description = new JTextArea(gainDescription);
                description.setEditable(false);
                description.setLineWrap(true);
                description.setWrapStyleWord(true);
                description.setSize(200, 230);
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.anchor = GridBagConstraints.CENTER;
                gainPanel.add(description, gbc);
                gbc.gridx = 0;
                gbc.gridy = 2;
                gbc.anchor = GridBagConstraints.CENTER;
                sliderArray[1] = new JSlider(0, 10, 5);
                sliderArray[1].setLabelTable(labels);
                sliderArray[1].setPaintLabels(true);
                sliderArray[1].setPaintTicks(true);
                sliderArray[1].addChangeListener((ChangeEvent e) ->
                {
                    // Get the current value of the slider.
                    selectedVals[1] = ((JSlider) e.getSource()).getValue();
                });
                
                // Add silderArray at pos 1 to the gainPanel.  
                gainPanel.add(sliderArray[1], gbc);
                
                // Add the gainPanel to the GUI.
                add(gainPanel);

                // A panel for the presence setting.
                JPanel presencePanel = new JPanel(new GridBagLayout());
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;
                presencePanel.add(new JLabel("<HTML><U> Presence </U></HTML>"),
                        gbc);
                // Display the description of Presence.
                description = new JTextArea(presenceDescription);
                description.setEditable(false);
                description.setLineWrap(true);
                description.setWrapStyleWord(true);
                description.setSize(200, 230);
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.anchor = GridBagConstraints.CENTER;
                presencePanel.add(description, gbc);
                gbc.gridx = 0;
                gbc.gridy = 2;
                gbc.anchor = GridBagConstraints.CENTER;
                sliderArray[2] = new JSlider(0, 10, 5);
                sliderArray[2].setLabelTable(labels);
                sliderArray[2].setPaintLabels(true);
                sliderArray[2].setPaintTicks(true);
                sliderArray[2].addChangeListener((ChangeEvent e) ->
                {
                    // Get the current value of the slider.
                    selectedVals[2] = ((JSlider) e.getSource()).getValue();
                });
                
                // Add the sliderArray at pos 2 to the presencePanel.
                presencePanel.add(sliderArray[2], gbc);
                
                // Add the presencePanel to the GUI.
                add(presencePanel);

                // A panel for the mid setting.
                JPanel midPanel = new JPanel(new GridBagLayout());
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;
                midPanel.add(new JLabel("<HTML><U> Middle </U></HTML>"),
                        gbc);
                // Display the description of Middle.
                description = new JTextArea(middleDescription);
                description.setEditable(false);
                description.setLineWrap(true);
                description.setWrapStyleWord(true);
                description.setSize(200, 230);
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.anchor = GridBagConstraints.CENTER;
                midPanel.add(description, gbc);
                gbc.gridx = 0;
                gbc.gridy = 2;
                gbc.anchor = GridBagConstraints.CENTER;
                sliderArray[3] = new JSlider(0, 10, 5);
                sliderArray[3].setLabelTable(labels);
                sliderArray[3].setPaintLabels(true);
                sliderArray[3].setPaintTicks(true);
                sliderArray[3].addChangeListener((ChangeEvent e) ->
                {
                    // Get the current value of the slider.
                    selectedVals[3] = ((JSlider) e.getSource()).getValue();
                });
                
                // Add the sliderArray at pos 3 to the midPanel.
                midPanel.add(sliderArray[3], gbc);
                
                // Add the midPanel to the GUI.
                add(midPanel);

                // A panel for the bass setting.
                JPanel bassPanel = new JPanel(new GridBagLayout());
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;
                bassPanel.add(new JLabel("<HTML><U> Bass </U></HTML>"),
                        gbc);
                
                // Display the description of Bass.
                description = new JTextArea(bassDescription);
                description.setEditable(false);
                description.setLineWrap(true);
                description.setWrapStyleWord(true);
                description.setSize(200, 230);
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.anchor = GridBagConstraints.CENTER;
                bassPanel.add(description, gbc);
                gbc.gridx = 0;
                gbc.gridy = 2;
                gbc.anchor = GridBagConstraints.CENTER;
                sliderArray[4] = new JSlider(0, 10, 5);
                sliderArray[4].setLabelTable(labels);
                sliderArray[4].setPaintLabels(true);
                sliderArray[4].setPaintTicks(true);
                sliderArray[4].addChangeListener((ChangeEvent e) ->
                {
                    // Get the current value of the slider.
                    selectedVals[4] = ((JSlider) e.getSource()).getValue();
                });
                
                // Add the sliderArray at pos 4 to the bassPanel.
                bassPanel.add(sliderArray[4], gbc);
                
                // Add the bassPanel to the GUI.
                add(bassPanel);
            }
        };
        
        // Add the eqPanel to the GUI.
        creationPanel.add(eqPanel);

        // Create a new JPanel called fieldsPanel that uses GBL.
        JPanel fieldsPanel = new JPanel(new GridBagLayout());
        
        // Create a new JPanel called artistNamePanel that uses GBL.
        JPanel artistNamePanel = new JPanel(new GridBagLayout());
        
        // Create a new JTextField called artistNameField with a char limit 20.
        JTextField artistNameField = new JTextField(20);
        
        // Set the default text of the artistNameField.
        artistNameField.setText("Enter the artists name...");
        artistNameField.setName("artistNameField");
        artistNameField.addFocusListener(new FocusAdapter()
        {
            // Once focus is gained, clear the text field.
            @Override
            public void focusGained(FocusEvent e)
            {
                artistNameField.setText("");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        
        // Add the new JLabel to artistNamePanel.
        artistNamePanel.add(new JLabel("Artist Name: "), gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        
        // Add the new artistNameField to the artistNamePanel.
        artistNamePanel.add(artistNameField, gbc);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        
        // Add the artistNamePanel to the fieldsPanel.
        fieldsPanel.add(artistNamePanel, gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;

        // Create the ampNamePanel and ise the GBL.
        JPanel ampNamePanel = new JPanel(new GridBagLayout());
        JTextField ampNameField = new JTextField(20);
        ampNameField.setText("Enter the name of the amplifier...");
        ampNameField.setName("ampNameField");
        ampNameField.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusGained(FocusEvent e)
            {
                // Once focus is cagined clear the text field.
                ampNameField.setText("");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        ampNamePanel.add(new JLabel("Amplifier Name: "), gbc);
        
        // Add a new JPanel to the ampNamePanel.
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        ampNamePanel.add(ampNameField, gbc);
        
        // Add the ampNameField to the ampNamePanel. 
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.weightx = 5;
        fieldsPanel.add(ampNamePanel, gbc);
        
        // Add the ampNamePanel to the fieldsPanel.
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        creationPanel.add(fieldsPanel, gbc);

        // Create the actionsPanel.
        JPanel actionsPanel = new JPanel();
        // Create the createButton button to create a setting.
        JButton createButton = new JButton("Create Setting");
        createButton.addActionListener((ActionEvent e) ->
        {

            if (artistNameField.getText().equals("Enter the artists name...")
                    || artistNameField.getText().equals("")
                    || ampNameField.getText().equals("Enter the name of "
                            + "the amplifier...") || ampNameField.getText()
                            .equals(""))
            {
                JOptionPane.showMessageDialog(null,
                        "Please fill in all fields.");
            } else
            {
                String query;
                int choice = JOptionPane.showConfirmDialog(null, "Are you sure "
                        + "you would like to create this setting?");
                if (choice == 0)
                {
                    query = "INSERT INTO AmpSettingz(ID, REVERB, GAIN, PRESENCE"
                            + ", MIDDLE, BASS, ARTIST_NAME, AMP_NAME)\nVALUES("
                            + (SERVER.getID() + 1) + ", " + selectedVals[0]
                            + ", " + selectedVals[1] + ", " + selectedVals[2]
                            + ", " + selectedVals[3] + ", " + selectedVals[4]
                            + ", '" + artistNameField.getText() + "', '"
                            + ampNameField.getText() + "')";
                    SERVER.executeQuery(query, "INSERT");
                    SERVER.incrementID();

                    // Set the edit and remove button to enabled.
                    editButton.setEnabled(true);
                    tableButton.setEnabled(false);
                    removeButton.setEnabled(true);

                    // Call the updateTable method.
                    updateTable();

                    // Set each slider's value to 5 (default).
                    for (JSlider slider : sliderArray)
                    {
                        slider.setValue(5);
                    }

                    // Set the two JTextField's text to the default.
                    artistNameField.setText("Enter the artist's name...");
                    ampNameField.setText("Enter the name of the amplifier...");

                    // Refresh the GUI.
                    validate();
                } else if (choice == 2)
                {
                    // Set each slider's value to 5 (default).
                    for (JSlider slider : sliderArray)
                    {
                        slider.setValue(5);
                    }

                    // Set the two JTextField's text to the default.
                    artistNameField.setText("Enter the artist's name...");
                    ampNameField.setText("Enter the name of the amplifier...");

                    // Call the updateTable method.
                    updateTable();
                }
            }
        });
        // Add the createButton to the actionsPanel.
        actionsPanel.add(createButton);

        // Create the cancelButton and add a action listener to it.
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((ActionEvent e) ->
        {
            // Set each slider's value to 5 (default).
            for (JSlider slider : sliderArray)
            {
                slider.setValue(5);
            }

            // Set the two JTextField's text to the default.
            artistNameField.setText("Enter the artist's name...");
            ampNameField.setText("Enter the name of the amplifier...");

            // If the server has data, set the edit and remove button to true.
            if (SERVER.hasData())
            {
                editButton.setEnabled(true);
                removeButton.setEnabled(true);
                tableButton.setEnabled(false);
            }

            // Call the updateTable() method.
            updateTable();
        });
        // Add the cancelButton to the actionsPanel.
        actionsPanel.add(cancelButton);
        
        // Add the actionsPanel to the creationPanel.
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        creationPanel.add(actionsPanel, gbc);
        
        // Add the creationPanel to the cards.
        cards.add(creationPanel, CREATION_PANEL_ID);
    }

    /**
     * This method initializes the buttonsPanel, which holds all of the buttons
     * that appear on the right side of the GUI.
     * <p>
     * CODE REFERENCED FROM: REFERENCED FROM: https://stackoverflow.com/
     * questions/789517/java-how-to-create-a-custom-dialog-box
     */
    private void initButtonsPanel()
    {
        // Create a new JPanel that uses GBL.
        buttonsPanel = new JPanel(new GridBagLayout());
        // Set the borders.
        buttonsPanel.setBorder(BorderFactory.createTitledBorder("Actions"));

        // Create a new button to create a new setting.
        newButton = new JButton("New Setting");
        
        // Set the preffered size of the button to 150 x 25.
        newButton.setPreferredSize(new Dimension(150, 25));
        newButton.addActionListener((ActionEvent e) ->
        {
            tableButton.setEnabled(true);
            editButton.setEnabled(false);
            removeButton.setEnabled(false);
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, CREATION_PANEL_ID);
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weighty = 0.0;
        gbc.insets = new Insets(2, 2, 2, 2);
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the newButton to the ButtonsPanel.
        buttonsPanel.add(newButton, gbc);

        // Create the new editButton.
        editButton = new JButton("Edit Setting");
        // Set the preferred size to 150 x 25. 
        editButton.setPreferredSize(new Dimension(150, 25));
        
        if (SERVER.hasData())
            editButton.setEnabled(false);
        editButton.addActionListener((ActionEvent e) ->
        {
            // Create a new JPanel that allows the user to chooser a setting to
            // edit.
            JPanel grabberPanel = new JPanel(new GridBagLayout());
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.CENTER;
            // Add a new JLabel to the grabberPanel.  
            grabberPanel.add(new JLabel("Choose the ID of The Setting to Edit"),
                    gbc);

            /* Create an array of Strings that shows all of the available 
            choices from the database, then populate it. */
            String[] ids = new String[SERVER.getID()];
            for (int i = 0; i < ids.length; i++)
                ids[i] = String.valueOf(i + 1);

            // Create a new JPanle called choicePanel that uses GBL.
            JPanel choicePanel = new JPanel(new GridBagLayout());
            JComboBox idBox = new JComboBox(ids);
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new JLabel that contains the ID to the choicePanel.
            choicePanel.add(new JLabel("ID: "), gbc);
            
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the idBox to the ChoicePanel.
            choicePanel.add(idBox, gbc);

            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.anchor = GridBagConstraints.CENTER;
            // Add the choicePanel to the grabbeerPanel.
            grabberPanel.add(choicePanel, gbc);

            // Pop a dialog for the user to choose the setting.
            Object[] options = {
                "Edit", "Cancel"
            };
            
            // Initialize int ID as 0.
            int ID = 0;
            
            // If the selected value to change is correct, set the ID to the int
            // value of that.
            if (JOptionPane.showOptionDialog(null, grabberPanel,
                    "Edit Setting", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE, null, options, null) == 0)
            {
                ID = Integer.valueOf(String.valueOf(idBox.getSelectedItem()));
            } 
            else
            {
                return;
            }

            // Get the row of data from the server.
            Object[] rowData = SERVER.getRow(ID);

            // Create a new JComboBox with length of 5.
            JComboBox[] comboBoxArr = new JComboBox[5];

            // Populate the fillerData array.
            final String[] fillerData = {
                "0", "1", "2", "3", "4", "5", "6",
                "7", "8", "9", "10"
            };

            // Traverse through the comboBoxArr and set the selected index of 
            // every row.
            for (int i = 0; i < comboBoxArr.length; i++)
            {
                comboBoxArr[i] = new JComboBox(fillerData);
                comboBoxArr[i].setSelectedIndex(Integer.parseInt(String
                        .valueOf(rowData[i])));
            }

            // Create a new JPanel called panel that uses GBL.
            JPanel panel = new JPanel(new GridBagLayout());

            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_END;
            gbc.insets = new Insets(1, 1, 1, 1);
            // Add a new "Reverb Panel" to the panel.
            panel.add(new JLabel("Reverb: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the comboBoxArr at pos 0 to the panel.
            panel.add(comboBoxArr[0], gbc);

            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "Gain Panel" to the panel.
            panel.add(new JLabel("Gain: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 1;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the comboBoxArr at pos 1 to the panel.
            panel.add(comboBoxArr[1], gbc);

            gbc.gridx = 0;
            gbc.gridy = 2;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "Presence Panel" to the panel.
            panel.add(new JLabel("Presence: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 2;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the comboBoxArr at pos 2 to the panel.
            panel.add(comboBoxArr[2], gbc);

            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "Middle Panel" to the panel.
            panel.add(new JLabel("Middle: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 3;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the comboBoxArr at pos 3 to the Panel.
            panel.add(comboBoxArr[3], gbc);

            gbc.gridx = 0;
            gbc.gridy = 4;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "Bass Panel" to the panel.
            panel.add(new JLabel("Bass: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 4;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the comboBoxArr at pos 4 to the panel.
            panel.add(comboBoxArr[4], gbc);

            // Create the artistNameField with char length of 20.
            JTextField artistNameField = new JTextField(20);
            artistNameField.setText(String.valueOf(rowData[6]));
            gbc.gridx = 0;
            gbc.gridy = 5;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "ArtistName Panel" to the panel.
            panel.add(new JLabel("Artist Name: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 5;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the artistNameField to the panel.
            panel.add(artistNameField, gbc);

            // Create the ampNameField with char length of 20.
            JTextField ampNameField = new JTextField(20);
            ampNameField.setText(String.valueOf(rowData[7]));
            gbc.gridx = 0;
            gbc.gridy = 6;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add a new "AmplifierName Panel" to the panel.
            panel.add(new JLabel("Amplifier Name: "), gbc);
            gbc.gridx = 1;
            gbc.gridy = 6;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the ampNameField to the panel.
            panel.add(ampNameField, gbc);

            // Create an array of Strings called options.
            options = new String[]
            {
                "Edit Setting", "Cancel"
            };
            
            // Create an result integer that stroes the edited setting.
            int result = JOptionPane.showOptionDialog(null, panel,
                    "Edit Setting", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE, null, options, null);

            // If the user selects "ok" set the new values of the AmpSettingz.
            if (result == JOptionPane.OK_OPTION)
            {
                final String reverb = String.valueOf(comboBoxArr[0]
                        .getSelectedItem());
                final String gain = String.valueOf(comboBoxArr[1]
                        .getSelectedItem());
                final String presence = String.valueOf(comboBoxArr[2]
                        .getSelectedItem());
                final String middle = String.valueOf(comboBoxArr[3]
                        .getSelectedItem());
                final String bass = String.valueOf(comboBoxArr[4]
                        .getSelectedItem());
                final String artistName = "'" + artistNameField.getText() + "'";
                final String ampName = "'" + ampNameField.getText() + "'";

                // Create a new String that displays all the Amp Settingz.
                String query = "UPDATE AMPSETTINGZ \n"
                        + "SET REVERB = " + reverb + ", GAIN = " + gain
                        + ", PRESENCE = " + presence + ", MIDDLE = " + middle
                        + ", BASS = " + bass + ", ARTIST_NAME = " + artistName
                        + ", AMP_NAME = " + ampName + "\n"
                        + "WHERE ID = " + ID;

                // Execute the query with the new values on the Database.
                SERVER.executeQuery(query, "UPDATE");

                // Call the updateTable() method.
                updateTable();
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the editButton to the buttonsPanel.
        buttonsPanel.add(editButton, gbc);

        // Create the removeButton.
        removeButton = new JButton("Remove Setting");
        // Set the size to 150 x 25.
        removeButton.setPreferredSize(new Dimension(150, 25));
        if (SERVER.hasData())
        {
            removeButton.setEnabled(false);
        }
        removeButton.addActionListener((ActionEvent e) ->
        {
            // Create a new JPanel called grabberPanel that uses GBL.
            JPanel grabberPanel = new JPanel(new GridBagLayout());
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.CENTER;
            // Add the new JLbale to the grabberPanel.
            grabberPanel.add(new JLabel("Choose the ID of The Setting to "
                    + "Remove"), gbc);

            /* Create an array of Strings that shows all of the available 
            choices from the database, then populate it. */
            String[] ids = new String[SERVER.getID() + 1];
            for (int i = 1; i < ids.length; i++)
            {
                ids[i] = String.valueOf(i);
            }
            ids[0] = "-";

            // Create the new JPanel called choicePanel that uses GBL.
            JPanel choicePanel = new JPanel(new GridBagLayout());
            JComboBox idBox = new JComboBox(ids);
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_END;
            // Add the new JLabel "ID" to the choicePanel.
            choicePanel.add(new JLabel("ID: "), gbc);
            
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.LINE_START;
            // Add the idBox to the choicePanel.
            choicePanel.add(idBox, gbc);
            
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.anchor = GridBagConstraints.CENTER;
            // Add the choicePanel to the grabberPanel.
            grabberPanel.add(choicePanel, gbc);

            // Pop a dialog for the user to choose the setting.
            Object[] options = { "Edit", "Cancel" };
            // Create the an int called ID and set it to 0.
            int ID = 0;
            if (JOptionPane.showOptionDialog(null, grabberPanel,
                    "Edit Setting", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.PLAIN_MESSAGE, null, options, null) == 0)
            {
                if (String.valueOf(idBox.getSelectedItem()).equals("-"))
                {
                    return;
                } else
                {
                    // Set the ID to the idBox's selected item.
                    ID = Integer.valueOf(String.valueOf(idBox
                            .getSelectedItem()));
                    // Create a String called query that deltes a setting.
                    String query = "DELETE FROM AmpSettingz\nWHERE ID = " + ID;
                    // Execute the query on the server and update the Database.
                    SERVER.executeQuery(query, "DELETE");

                    // Call the updateTable() method.
                    updateTable();
                }

                if (SERVER.hasData())
                {
                    editButton.setEnabled(false);
                }

                // Refresh the GUI.
                validate();
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the removeButton to the buttonsPanel.
        buttonsPanel.add(removeButton, gbc);

        // Create a new JButton called printButton.
        printButton = new JButton("Print Database");
        // Set the size to 150 x 25.
        printButton.setPreferredSize(new Dimension(150, 25));
        printButton.addActionListener((ActionEvent e) ->
        {
            // Print the DB.
            SERVER.printDB();
        });
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the printButton to the buttonsPanel.
        buttonsPanel.add(printButton, gbc);

        // Create the tableButton. 
        tableButton = new JButton("Show Table");
        // Set the size to 150 x 25.
        tableButton.setPreferredSize(new Dimension(150, 25));
        tableButton.setEnabled(false);
        tableButton.addActionListener((ActionEvent e) ->
        {
            // Call the updateTable() method.
            updateTable();
        });
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the tableButton to the buttonsPanel.
        buttonsPanel.add(tableButton, gbc);

        // Create the exitButton.
        exitButton = new JButton("Quit");
        // Set the size to 150 x 25.
        exitButton.setPreferredSize(new Dimension(150, 25));
        exitButton.addActionListener((ActionEvent e) ->
        {
            // Truncate the database.
            SERVER.dispatchDB();
            // Exit the program.
            System.exit(0);
        });
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.CENTER;
        // Add the exitButton to the buttonsPanel.
        buttonsPanel.add(exitButton, gbc);

        // Add the buttonsPanel to the GUI.
        add(buttonsPanel, BorderLayout.EAST);
    }

    /**
     * Initializes the basicInstructionsPanel.
     */
    private void initBasicInstructionsPanel()
    {
        // If the basicInstructionsPanel has been created previously, clear it.
        if (isBasicInstructionsPanelVisible)
        {
            remove(basicInstructionsPanel);
            basicInstructionsPanel = null;
        }

        // Creates a JPanel.
        basicInstructionsPanel = new JPanel();

        // Get the Instructions Panel object and add it to the GUI.
        JScrollPane instructionsPane = HTMLFactory
                .getInstructionsPanelInstance();
        basicInstructionsPanel.add(instructionsPane);

        // Add the basicInstructionsPanel to the cards.
        cards.add(basicInstructionsPanel, BASIC_INSTRUCTION_ID);
        
        // Refresh the GUI.
        validate();
    }

    /**
     * Initializes the aboutPanel.
     */
    private void initAboutPanel()
    {
        // If the aboutPanel has been created previously, clear it.
        if (isAboutPanelVisible)
        {
            remove(aboutPanel);
            aboutPanel = null;
        }

        // Creates a JPanel.
        aboutPanel = new JPanel();

        // Get the Info Panel object and add it to the GUI.
        JScrollPane instructionsPane = HTMLFactory
                .getBasicInfoPanelInstance();
        aboutPanel.add(instructionsPane);

        // Add the aboutPanel to cards.
        cards.add(aboutPanel, ABOUT_ID);
        
        // Refresh the GUI.
        validate();
    }

    /**
     * This creates a JMenu that has a File, Edit, View, and Help Menu.
     * https://docs.oracle.com/javase/tutorial/uiswing/components/menu.html
     */
    private void setMenu()
    {
        // Where the GUI is created:
        JMenuItem menuItem;

        // Create the menu bar.
        menuBar = new JMenuBar();

        // Create File JMenu.
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_A);

        // Create "New Setting" for fileMenu.
        menuItem = new JMenuItem("New Setting");
        menuItem.addActionListener((ActionEvent e) -> {
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, CREATION_PANEL_ID);
        });
        fileMenu.add(menuItem);

        // Add a seperator line.
        fileMenu.addSeparator();

        // Create "Open Database" for fileMenu.
        menuItem = new JMenuItem("Open Database");
        menuItem.addActionListener((ActionEvent e) -> {
            if (SERVER.hasData())
                SERVER.dispatchDB();
            SERVER.loadDB();
            editButton.setEnabled(true);
            tableButton.setEnabled(false);
            removeButton.setEnabled(true);
            updateTable();
        });
        fileMenu.add(menuItem);

        // Create "Save Database" for fileMenu.
        menuItem = new JMenuItem("Save Database");
        menuItem.addActionListener((ActionEvent e) -> {
            SERVER.saveDB();

            // Swap back to the table.  
            editButton.setEnabled(true);
            tableButton.setEnabled(false);
            removeButton.setEnabled(true);
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, TABLE_PANEL_ID);
        });
        fileMenu.add(menuItem);

        // Add a seperator line.
        fileMenu.addSeparator();

        // Create "Print" for fileMenu.
        menuItem = new JMenuItem("Print");
        menuItem.addActionListener((ActionEvent e) -> {
            // Call the server's printDB() method.
            SERVER.printDB();
        });
        fileMenu.add(menuItem);

        // Add a seperator line.
        fileMenu.addSeparator();

        // Create "Exit" for fileMenu.
        menuItem = new JMenuItem("Exit");
        menuItem.addActionListener((ActionEvent e) -> {
            SERVER.dispatchDB();
            System.exit(0);
        });
        fileMenu.add(menuItem);

        // Create Edit JMenu.
        JMenu editMenu = new JMenu("Edit");
        editMenu.setMnemonic(KeyEvent.VK_A);

        // Create "Edit Setting(s)" for editMenu.
        menuItem = new JMenuItem("Edit Setting");
        menuItem.addActionListener((ActionEvent e) -> {
            editButton.doClick();
        });
        editMenu.add(menuItem);

        // Create "Remove Setting(s)" for editMenu.
        menuItem = new JMenuItem("Remove Setting");
        menuItem.addActionListener((ActionEvent e) -> {
            removeButton.doClick();
        });
        editMenu.add(menuItem);

        // Create View JMenu.
        JMenu viewMenu = new JMenu("View");
        viewMenu.setMnemonic(KeyEvent.VK_A);

        // Create "View Table" for viewMenu.
        menuItem = new JMenuItem("Show Table");
        menuItem.addActionListener((ActionEvent e) ->  {
            tableButton.setEnabled(false);
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, TABLE_PANEL_ID);
        });
        viewMenu.add(menuItem);

        // Create Help JMenu.
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_A);

        // Create "About" JMenuItem for helpMenu.
        menuItem = new JMenuItem("About");
        menuItem.addActionListener((ActionEvent e) -> {
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, ABOUT_ID);
        });
        helpMenu.add(menuItem);

        // Seperator line.
        helpMenu.addSeparator();

        // Create "Basic Instructions" JMenuItem for helpMenu.
        menuItem = new JMenuItem("Basic Instructions");
        menuItem.addActionListener((ActionEvent e) -> {
            CardLayout cL = (CardLayout) cards.getLayout();
            cL.show(cards, BASIC_INSTRUCTION_ID);
        });
        helpMenu.add(menuItem);

        // Set all bars to visible and add it to the menuBar.
        menuBar.setVisible(true);
        fileMenu.setVisible(true);
        menuBar.add(fileMenu);
        editMenu.setVisible(true);
        menuBar.add(editMenu);
        viewMenu.setVisible(true);
        menuBar.add(viewMenu);
        helpMenu.setVisible(true);
        menuBar.add(helpMenu);

        // Set the JMenuBar to menuBar.
        setJMenuBar(menuBar);

        // Refresh the GUI.
        validate();
    }

    /**
     * Sets the frame and icon banner of the program.
     */
    private void setFrameIconAndBanner()
    {
        // Createa a new ImageIcon object and set it to the image icon.
        ImageIcon icon = new ImageIcon("AMPlogo.png");
        setIconImage(icon.getImage());

        // Create a new JPanel called the bannerPanel.
        JPanel bannerPanel = new JPanel();
        
        // Try get read the file banner.png and add it to the bannerPanel.
        try
        {
            bannerPanel.add(new JLabel(new ImageIcon(ImageIO
                    .read(new File("banner.png")))));
        } catch (IOException ex)
        {
            System.err.println("Error reading banner.png from project "
                    + "directory. Was it possibly deleted?");
        }

        // Add the bannerPanel to the GUI.
        add(bannerPanel, BorderLayout.NORTH);
    }

    /**
     * Updates the current table.
     */
    private void updateTable()
    {
        // Remove the scrollPane and it's contents, then destroy them.
        cards.remove(tableScrollPane);
        tableScrollPane.remove(table);
        table = null;
        remove(tableScrollPane);
        tableScrollPane = null;

        // Recreate the Table.
        table = new Table(SERVER.dbToArray());
        tableScrollPane = new JScrollPane(table);
        cards.add(tableScrollPane, TABLE_PANEL_ID);

        // Switch to the Table Panel.
        CardLayout cL = (CardLayout) cards.getLayout();
        cL.show(cards, TABLE_PANEL_ID);
    }
}